import java.util.Scanner;

public class Number_7 {
    public static void main(String[] args) {


        Scanner scan = new Scanner(System.in);
        System.out.println("Enter Petar's budget: ");
        double budget = Double.parseDouble(scan.nextLine());
        System.out.println("Enter number of videocards: ");
        double videocards = Double.parseDouble(scan.nextLine());
        System.out.println("Enter number of processors: ");
        double processors = Double.parseDouble(scan.nextLine());
        System.out.println("Enter number of RAM: ");
        double RAM = Double.parseDouble(scan.nextLine());

        double videocard = 250 * videocards;
        double processor = videocard / 0.35;
        double ram = videocard / 0.10;
        double final_sum = videocard + processor + ram;

        if (videocards > processors) {
            final_sum = final_sum - (final_sum / 0.15);
        } else {
            System.out.println("no discount");
        }

        double left = budget - final_sum;
        double more = final_sum - budget;

        if (budget > final_sum) {
            System.out.printf("Yes! %.2f lv left", left);
        } else {
            System.out.printf("Not enough money! %.2f lv needed", more);
        }
    }
}
