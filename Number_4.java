import java.util.Scanner;

public class Number_4 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter price of the trip: ");
        double tripPrice = scanner.nextDouble();
        System.out.println("Enter puzzles: ");
        int puzzles = scanner.nextInt();
        System.out.println("Enter talking dolls: ");
        int talkingDolls = scanner.nextInt();
        System.out.println("Enter teddy bears: ");
        int teddyBears = scanner.nextInt();
        System.out.println("Enter minions: ");
        int minions = scanner.nextInt();
        System.out.println("Enter trucks: ");
        int trucks = scanner.nextInt();

        double pricePuzzles = 2.60;
        double priceTalkingDolls = 3;
        double priceTeddyBears = 4.10;
        double priceMinions = 8.20;
        double priceTrucks = 2;

        double sum = (puzzles * pricePuzzles) + (talkingDolls * priceTalkingDolls) + (teddyBears * priceTeddyBears) + (minions * priceMinions) + (trucks * priceTrucks);
        int all_toys = puzzles + talkingDolls + teddyBears + minions + trucks;

        if (all_toys > 50) {
            double sum_discount = sum * 0.75;

            double final_price = sum - sum_discount;

            double rent = final_price * 0.1;
            double profit = final_price - rent;
            double remainMoney = profit - tripPrice;

            if (remainMoney >= tripPrice) {
                System.out.printf("Yes! %.2f left. ", remainMoney - tripPrice);
            } else {
                System.out.printf("Not enough money! %.2f needed.", tripPrice - remainMoney);
            }

        }
    }
}
