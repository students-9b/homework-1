import java.util.Scanner;

public class Number_3 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int hours = Integer.parseInt(scanner.nextLine());
        int minutes = Integer.parseInt(scanner.nextLine());

        int newHour;
        int newMinutes;

        if (minutes + 15 < 60) {
            newHour = hours;
            newMinutes = minutes + 15;
        } else {
            newHour = (hours + 1) % 24;
            newMinutes = (minutes + 15) % 60;
        }
        System.out.printf("Час след 15 минути: %02d:%02d%n", newHour, newMinutes);
    }
}