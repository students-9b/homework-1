import java.util.Scanner;

public class Number_6 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter Ivan's record: ");
        double Ivan_record = scanner.nextDouble();

        System.out.println("Enter record in metres: ");
        double record_metres = scanner.nextDouble();
        System.out.println("Enter time in seconds: ");
        double time_seconds = scanner.nextDouble();
        double the_record = record_metres * time_seconds ;
        double fifteen_metres = record_metres / 15;
        double fifteen_metres_continue = fifteen_metres * 12.5;
        double all_time = the_record + fifteen_metres_continue;


        if (Ivan_record<all_time) {
            System.out.println("Yes, he succeeded! The new world record is Ivan_record seconds.");
        } else {
            System.out.println("No, he failed! He was all_time - Ivan_record seconds slower.");
        }
    }
}
